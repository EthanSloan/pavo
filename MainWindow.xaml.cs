﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using Windows.Foundation;
using Windows.UI.ViewManagement;
using ABI.Windows.UI.ViewManagement;
using Microsoft.UI.Xaml;
using Microsoft.UI.Xaml.Controls;
using static PInvoke.User32;
using System.Threading.Tasks;

// To learn more about WinUI, the WinUI project structure,
// and more about our project templates, see: http://aka.ms/winui-project-info.

namespace Pavo
{
    /// <summary>
    /// An empty window that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private async void Num1_Click(object sender, RoutedEventArgs e)
        {
            var oneProgramFound = await IsProcessRunningAsync("notepad++");
            if (oneProgramFound) PressKey(ScanCode.NUMPAD1);
        }

        private async void Num2_Click(object sender, RoutedEventArgs e)
        {
            var oneProgramFound = await IsProcessRunningAsync("notepad++");
            if (oneProgramFound) PressKey(ScanCode.NUMPAD2);
        }

        private async void Num3_Click(object sender, RoutedEventArgs e)
        {
            var oneProgramFound = await IsProcessRunningAsync("notepad++");
            if (oneProgramFound) PressKey(ScanCode.NUMPAD3);
        }

        private async void Num4_Click(object sender, RoutedEventArgs e)
        {
            var oneProgramFound = await IsProcessRunningAsync("notepad++");
            if (oneProgramFound) PressKey(ScanCode.NUMPAD4);
        }

        private async void Num5_Click(object sender, RoutedEventArgs e)
        {
            var oneProgramFound = await IsProcessRunningAsync("notepad++");
            if (oneProgramFound) PressKey(ScanCode.NUMPAD5);
        }

        private async void Num6_Click(object sender, RoutedEventArgs e)
        {
            var oneProgramFound = await IsProcessRunningAsync("notepad++");
            if (oneProgramFound) PressKey(ScanCode.NUMPAD6);
        }

        private async void Num7_Click(object sender, RoutedEventArgs e)
        {
            var oneProgramFound = await IsProcessRunningAsync("notepad++");
            if (oneProgramFound) PressKey(ScanCode.NUMPAD7);
        }

        private async void Num8_Click(object sender, RoutedEventArgs e)
        {
            var oneProgramFound = await IsProcessRunningAsync("notepad++");
            if (oneProgramFound) PressKey(ScanCode.NUMPAD8);
        }

        private async void Num9_Click(object sender, RoutedEventArgs e)
        {
            var oneProgramFound = await IsProcessRunningAsync("notepad++");
            if (oneProgramFound) PressKey(ScanCode.NUMPAD9);
        }

        private async void Num0_Click(object sender, RoutedEventArgs e)
        {
            var oneProgramFound = await IsProcessRunningAsync("notepad++");
            if (oneProgramFound) PressKey(ScanCode.NUMPAD0);
        }

        // private void Example_Click(object sender, RoutedEventArgs e)
        // {
        //     var processArray = Process.GetProcessesByName("notepad++");
        //     SetForegroundWindow(processArray[0].MainWindowHandle);
        //
        //     HoldKey(ScanCode.SHIFT);
        //     PressKey(ScanCode.KEY_W);
        //     ReleaseKey(ScanCode.SHIFT);
        //     PressKey(ScanCode.KEY_O);
        //     PressKey(ScanCode.KEY_W);
        //     PressKey(ScanCode.OEM_PERIOD);
        //     PressKey(ScanCode.SPACE);
        // }

        private async Task<bool> IsProcessRunningAsync(string processName)
        {
            var processArray = Process.GetProcessesByName(processName);
            switch (processArray.Length)
            {
                case 0:
                {
                    var noProcess = new ContentDialog()
                    {
                        Title = "App not found",
                        Content = "The app may not be running.",
                        CloseButtonText = "OK",
                        XamlRoot = Content.XamlRoot
                    };
                    await noProcess.ShowAsync();

                    return false;
                }
                case 1:
                {
                    SetForegroundWindow(processArray[0].MainWindowHandle);
                    return true;
                }
                case > 1:
                {
                    var multipleProcesses = new ContentDialog()
                    {
                        Title = "Too many apps found",
                        Content = "Only one app instance is currently supported.",
                        CloseButtonText = "OK",
                        XamlRoot = Content.XamlRoot
                    };
                    await multipleProcesses.ShowAsync();

                    return false;
                }
                default:
                    return false;
            }
        }

        private static void PressKey(ScanCode keyCode)
        {
            INPUT[] inputs = {
                new INPUT
                {
                    type = InputType.INPUT_KEYBOARD,
                    Inputs = new INPUT.InputUnion
                    {
                        ki = new KEYBDINPUT
                        {
                            wVk = 0,
                            wScan = keyCode,
                            dwFlags = 0 | KEYEVENTF.KEYEVENTF_SCANCODE,
                        }
                    }
                },
                new INPUT
                {
                    type = InputType.INPUT_KEYBOARD,
                    Inputs = new INPUT.InputUnion
                    {
                        ki = new KEYBDINPUT
                        {
                            wVk = 0,
                            wScan = keyCode,
                            dwFlags = KEYEVENTF.KEYEVENTF_KEYUP | KEYEVENTF.KEYEVENTF_SCANCODE,
                        }
                    }
                },
            };

            SendInput(inputs.Length, inputs, Marshal.SizeOf(typeof(INPUT)));
        }

        private static void HoldKey(ScanCode keyCode)
        {
            INPUT[] inputs = {
                new INPUT
                {
                    type = InputType.INPUT_KEYBOARD,
                    Inputs = new INPUT.InputUnion
                    {
                        ki = new KEYBDINPUT
                        {
                            wVk = 0,
                            wScan = keyCode,
                            dwFlags = 0 | KEYEVENTF.KEYEVENTF_SCANCODE,
                        }
                    }
                }
            };

            SendInput(inputs.Length, inputs, Marshal.SizeOf(typeof(INPUT)));
        }

        private static void ReleaseKey(ScanCode keyCode)
        {
            INPUT[] inputs = {
                new INPUT
                {
                    type = InputType.INPUT_KEYBOARD,
                    Inputs = new INPUT.InputUnion
                    {
                        ki = new KEYBDINPUT
                        {
                            wVk = 0,
                            wScan = keyCode,
                            dwFlags = KEYEVENTF.KEYEVENTF_KEYUP | KEYEVENTF.KEYEVENTF_SCANCODE,
                        }
                    }
                },
            };

            SendInput(inputs.Length, inputs, Marshal.SizeOf(typeof(INPUT)));
        }
    }
}
